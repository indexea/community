# Indexea

## Introduction

Indexea aims to build a search system building service, provide visually manages data, adjusts search criteria online, designs search ui interfaces without coding, and quickly delivers professional data search and analysis applications in a very short time.

## Contact us

If you are interested in indexea, please email us via indexea.com@gmail.com.
